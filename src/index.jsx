import { App } from './App'
import ReactDOM from 'react-dom'
import React from 'react'

window.timesLoaded = window.timesLoaded ? window.timesLoaded + 1 : 1;
if (module.hot && window.timesLoaded > 1) {
  window.location.reload();
} else {
  const div = document.createElement('div');
  document.body.appendChild(div);
  ReactDOM.render(<App/>, div);
}
